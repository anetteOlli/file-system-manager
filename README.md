# File System Manager

## Functionality:

1. list all the file names in a given directory.
2. get specific files by their extension.
3. Manipulate the provided txt
   1. See the name (or change it)
   2. Show how big the file is
   3. How many lines the file has.
   4. Let me seach if a specific word exists in the file.
   5. How many times the specific word exists in the file.
   6. the search must ignore case.
4. Logging function calls to a file
   1. Logs the function, function results and timestamp, duration of the function.

## Exception handling strategy:

- If the user can do something about them, for instance type in different user input:
  The exception will be thrown to the UI.
- If the user can't do anything about them, the exception is printet to the console.

# Compiling guide without EDI:

in root folder:

```sh
javac -d out src/main/*.java src/main/text/*.java src/main/ui/*.java src/main/browseDirectory/*.java src/main/loggerService/*.java
```

![compiling screenshot](images/javac.PNG)

## Creating jar file:

in out folder:

```sh
jar cfe FileSystemManager.jar Program *.class text/*.class ui/*.class resources/* browseDirectory/*.class loggerService/*.class
```

![generating jar screenshot](images/jar.PNG)

## Running the program:

```sh
java -jar .\FileSystemManager.jar

```

![running java](images/java.PNG)
