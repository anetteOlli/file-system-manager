package ui;

import java.util.Scanner;
import text.*;
import loggerService.LoggerService;



public class TextMenu {
    private Scanner sc;

    /**
     * @param sc java.util.Scanner, from where the menu will receive its user inputs.
     */
    public TextMenu(Scanner sc) {
        this.sc = sc;
    }


    /**
     * Main menu for the text file menu
     */
    public void showOptions() {
        boolean shouldContinue = true;
        while (shouldContinue) {
            System.out.println("\n### TEXT FILE MENU ###");
            System.out.println("1. Show name of file");
            System.out.println("2. Show size of file");
            System.out.println("3. Show number of lines in file");
            System.out.println("4. Word menu");
            System.out.println("5. Back");

            boolean ask = true;
            int option = 0;
            while ( ask ) {
                if ( sc.hasNext() && !sc.hasNextInt() ) {
                    sc.next();
                    System.out.println("type a number from the menu");
                }else if ( sc.hasNextInt() ) {
                    ask = false;
                    option = sc.nextInt();
                }
            }

            switch ( option ) {
                case 1: 
                //Show name of file
                {
                    FileMetaData fileMetaData = new FileMetaData();
                    Long startTime = System.currentTimeMillis();
                    String filename = fileMetaData.getFilename();
                    Long endTime = System.currentTimeMillis();
                    Long duration = endTime - startTime;
                    if(filename.length() == 0){
                        System.out.println("file not found");
                    }else {
                        System.out.println("Filename is " + filename + ", the function used " + duration + " milli seconds");
                    }
                    LoggerService loggerService = new LoggerService();
                    loggerService.appendLog("Collected filename. Filename was " + filename + ". Function used " + duration + " milli seconds");

                }
                break;

                case 2:
                //Show size of file
                {
                    FileMetaData fileMetaData = new FileMetaData();
                    Long startTime = System.currentTimeMillis();
                    Long fileSize = fileMetaData.fileSize();
                    Long endTime = System.currentTimeMillis();
                    Long duration = endTime - startTime;
                    if(fileSize == -1) {
                        System.out.println("File does not exists or you do not have access to it");
                    }else {
                        System.out.println("File is " + (fileSize / 1024  ) + " kibibytes large, the function used " + duration + " milli seconds");
                    }
                    LoggerService loggerService = new LoggerService();
                    loggerService.appendLog("Collected filesize. Filesize was " + fileSize + ". Function used " + duration + " milli seconds");
                }
                break;

                case 3:
                //Show number of lines in file
                {
                    LineCounter lineCounter = new LineCounter();
                    long startTime = System.currentTimeMillis();
                    int numOfLines = lineCounter.countLines();
                    long endTime = System.currentTimeMillis();
                    long duration = endTime - startTime;
                    if(numOfLines >= 0){
                        System.out.println("The file has " + numOfLines + " line(s), the function used " + duration + " milli seconds");
                    }else {
                        System.out.println("Could not recieve correct line count");
                    }
                    LoggerService loggerService = new LoggerService();
                    loggerService.appendLog("Counted lines in text file. Number of lines was: " + numOfLines + ". Function used " + duration + " milli seconds");

                }
                break;

                case 4:
                //show submenu for wordsearching
                this.wordMenu();
                break;

                case 5:
                //back
                shouldContinue = false;
                break;

                default:
                break;
            }
        }

    }

    /**
     * Sub menu for word searching in text file:
     */
    public void wordMenu() {
        boolean shouldContinue = true;

        while (shouldContinue) {
            System.out.println("\n### WORD MENU ###");
            System.out.println("1. How many times does word / sentence exist in file");
            System.out.println("2. Does word / sentence exist in file");
            System.out.println("3. back");
            boolean ask = true;
            int option = 0;

            while ( ask ) {
                if ( sc.hasNext() && !sc.hasNextInt() ) {
                    sc.next();
                    System.out.println("type a number from the menu");
                }else if ( sc.hasNextInt() ) {
                    ask = false;
                    option = sc.nextInt();
                }
            }

            switch ( option ) {
                case 1:
                {
                    System.out.println("Type the word you wich to search for");
                    WordSearch wordSearch = new WordSearch();
                    ask = true;
                    String word = "";
                    while (ask) {
                        if(sc.hasNext()){
                            ask = false;
                            word = sc.next();
                        }
                    }
                    long startTime = System.currentTimeMillis();
                    int wordCount = wordSearch.wordCount(word);
                    long endTime = System.currentTimeMillis();
                    long duration = endTime - startTime;
                    System.out.println("Found " + word + " " + wordCount + " times, used " + duration + " milli seconds" );
                    LoggerService loggerService = new LoggerService();
                    loggerService.appendLog("Found " + word + " " + wordCount + " times, used " + duration + " milli seconds. Function used " + duration + " milli seconds");

                }
                break;

                case 2: 
                    {
                    System.out.println("Type the word you wich to search for");
                    WordSearch wordSearch = new WordSearch();
                    ask = true;
                    String word = "";
                    while (ask) {
                        if(sc.hasNext()){
                            ask = false;
                            word = sc.next();
                        }
                    }
                    try {
                        long startTime = System.currentTimeMillis();
                        boolean textDidContainWord = wordSearch.containWord(word);
                        long endTime = System.currentTimeMillis();
                        long duration = endTime - startTime;
                        if (textDidContainWord) {
                            System.out.println("Found " + word + " in text. Search took: " + duration + " milli seconds");
                        } else {
                            System.out.println("Did not find " + word + " in text. Search took: " + duration + " milli seconds");
                        }
                        LoggerService loggerService = new LoggerService();
                        loggerService.appendLog("Searched for " + word + ", result was " + textDidContainWord +  ". Function used " + duration + " milli seconds");
                    }catch (IllegalArgumentException e) {
                        System.out.println("invalid search word");
                        e.printStackTrace();
                    }catch(Exception e) {
                        e.printStackTrace();
                    }
                }
                break;

                case 3: 
                shouldContinue = false;
                break;

                default:
                break;
            }
        }
    }
}