package browseDirectory;

import java.io.File;
import java.io.FilenameFilter;

public class BrowseDirectory {
    private final String WORKING_DIRECTORY = "./resources";

    /**
     * @return list of files in the resource folder
     */
    public String[] getAllfilesInCurrentDirectory(){
        try {
            File folder = new File(WORKING_DIRECTORY);
            return folder.list();
        }catch (SecurityException e) {
            e.printStackTrace();
            return new String[0];
        }
 
    }

    /**
     * Finds files by specified file ending in the resource folder.
     * @param fileEnding "." is not required
     * @return list of files, if 
     */
    public String[] getFilesByFileEnding(String fileEnding) {
        try {
            File folder = new File(WORKING_DIRECTORY);
            return folder.list(new FilenameFilter(){
                public boolean accept(File dir, String name) {
                    if(name.endsWith(fileEnding)){
                        return true;
                    }else {
                        return false;
                    }
                }
            });
        }catch (SecurityException e) {
            e.printStackTrace();
            return new String[0];
        }catch (Exception e) {
            e.printStackTrace();
            return new String[0];
        }
    }

    /**
     * Changes filename from oldname to newname
     * @param oldName old filename
     * @param newName new filename
     * @throws IllegalArgumentException if new name is invalid
     * @throws SecurityException if the user attempts to modify the Dracula.txt
     */
    public boolean changeName(String oldName, String newName) throws SecurityException, IllegalArgumentException {
        if(oldName.equals("Dracula.txt")){
            throw new SecurityException("Access denied for Dracula.txt"); 
        }
        boolean success = false;
        String path = "./resources/";
        File newFile = new File(path + newName);
        File oldFile = new File(path + oldName);
        try {
            success = oldFile.renameTo(newFile);
        }catch (SecurityException e) {
            System.out.println("Don't have access to file");
            e.printStackTrace();
        }catch (NullPointerException e) {
            throw new IllegalArgumentException("newName is invalid", e);
        }catch (Exception e) {
            e.printStackTrace();
        }
        return success;
    }
}
