import java.util.Scanner;
import ui.*;
import browseDirectory.BrowseDirectory;
import loggerService.LoggerService;


public class Program {

    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in);

        boolean shouldShowMenu = true;

        while ( shouldShowMenu ) {
            System.out.println("\n### MAIN MENU ###");
            System.out.println("Select an option:");
            System.out.println("1. View the files in the directory");
            System.out.println("2. Get filenames by exension name");
            System.out.println("3. Change filename of existing file");
            System.out.println("4. Manipulate the txt file");
            System.out.println("5. Exit");

            int result = 0;

            boolean ask = true;
            while ( ask ) {
                if ( sc.hasNext() && !sc.hasNextInt() ) {
                    sc.next();
                }else if ( sc.hasNextInt() ) {
                    ask = false;
                    result = sc.nextInt();
                }
            }

            switch ( result ) {
                case 1: {
                    //1. View the files in the directory
                    BrowseDirectory bd = new BrowseDirectory();
                    Long startTime = System.currentTimeMillis();
                    String[] fileList = bd.getAllfilesInCurrentDirectory();
                    Long endTime = System.currentTimeMillis();
                    String filesString = "";
                    for(String file : fileList) {
                        System.out.println(file);
                        filesString += file + " ";
                    }
                    Long duration = endTime - startTime;
                    System.out.println("The function used " + duration);
                    LoggerService loggerService = new LoggerService();
                    loggerService.appendLog("Listed all files in directory. Files found: " +  filesString + " Function used " + duration + " milliseconds");

                }
                break;

                case 2: 
                {
                    //2. Get filenames by exension name
                    System.out.println("Specify fileending name:");
                    boolean poll = true;
                    String fileEnding = "";
                    while (poll) {
                        if(sc.hasNext()){
                            fileEnding = sc.next();
                            poll = false;
                        }
                    }
                    
                    BrowseDirectory bd = new BrowseDirectory();
                    Long startTime  = System.currentTimeMillis();
                    String[] files = bd.getFilesByFileEnding(fileEnding);
                    Long endTime = System.currentTimeMillis();
                    Long duration = endTime - startTime;
                    String fileString = "";
                    for(String file: files) {
                        System.out.println(file);
                        fileString += file + " ";
                    }
                    System.out.println("The function used " + duration + " milli seconds");
                    LoggerService loggerService = new LoggerService();
                    loggerService.appendLog("Listed files with ending: " + fileEnding + "Found: " + fileString + ". Function used " + duration + " milli seconds");
                }
                break;
                
                case 3:
                    //3. Change filename of existing file
                {
                    System.out.println("Name of file you wish to change (Dracula.txt is not a valid option)");
                    String oldFilename = sc.next();
                    while (oldFilename.equals("Dracula.txt")){
                        System.out.println("You are not allowed to modify Dracula.txt");
                        oldFilename = sc.next();
                    }
                    System.out.println("New name of the file");
                    String newFilename = sc.next();
                    BrowseDirectory browseDirectory = new BrowseDirectory();
                    boolean success = false;
                    Long startTime = System.currentTimeMillis();
                    try {
                        success = browseDirectory.changeName(oldFilename, newFilename);
                    }catch (IllegalArgumentException e) {
                        System.out.println("No valid new filename submitted");
                        e.printStackTrace();
                    }
                    Long endTime = System.currentTimeMillis();
                    Long duration = endTime - startTime;
                    LoggerService loggerService = new LoggerService();
                    if (success) {
                        System.out.println("Filename changed. Function used " + duration);
                        loggerService.appendLog("Changed file " + oldFilename + " to " + newFilename + ". Function used " + duration + " milliseconds");
                    }else {
                        System.out.println("Failed to change filename, make sure old filename was spelled correctly");
                        loggerService.appendLog("Failed to change filename. Tried to change " + oldFilename + " to " + newFilename + ". Function used " + duration + " milliseconds");
                    }
                }
                break;
                    
                case 4:
                    //4. Manipulate the txt file
                    TextMenu textMenu = new TextMenu(sc);
                    textMenu.showOptions();
                break; 

                case 5:
                    //5. Exit
                    shouldShowMenu = false;
                break;

                default:
                break;
            }
        }
        sc.close();
    }
}