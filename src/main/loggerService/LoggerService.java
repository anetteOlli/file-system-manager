package loggerService;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.BufferedWriter;
import java.io.FileWriter;

public class LoggerService {
    private final String WORKING_DIRECTORY = "./logs/logger.txt";
    File file;
   

    /**
     * Constructor generates the log file if it does not exist.
     * May fail due to IO exceptions or if the systems denies the program access to it.
     */
    public LoggerService(){
        this.file = new File(WORKING_DIRECTORY);
        try {
            if(file.createNewFile()) {
                System.out.println("Log file created");
            }
        }catch (IOException e) {
            System.out.println("Couldn't create LoggerService due to IO exception");
            e.printStackTrace();
        }catch (SecurityException e) {
            System.out.println("Couldn't create LoggerService due to no priviligies");
            e.printStackTrace();
        }catch(Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @param log string message that will be logged
     * May fail to log due to IO exceptions.
     */
    public void appendLog(String log) {
        String localDateTime = java.time.LocalDateTime.now().toString();
        try (
            FileWriter fw = new FileWriter(this.file, true);
            BufferedWriter bw = new BufferedWriter(fw);
            PrintWriter pw = new PrintWriter(bw);
        ) {
            pw.println( localDateTime + "  " + log);
            pw.flush();
        }catch (IOException e) {
            System.out.println("Was not able to log");
            e.printStackTrace();
        }catch (Exception e){
            e.printStackTrace();
        } 
    }
}
