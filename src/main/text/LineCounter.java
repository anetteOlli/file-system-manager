package text;

import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;


public class LineCounter {
    private final String FILENAME = "./resources/Dracula.txt";

    /**
     * Count the number of lines in Dracula.txt
     * @return the number of lines in the text file. If an exception occurs it returns -1
     */
    public int countLines() {
        int numberOfLines = 0;

        try (Scanner sc = new Scanner(new File(this.FILENAME));) {
            while (sc.hasNextLine()){
                numberOfLines++;
                sc.nextLine();
            }
        }catch(FileNotFoundException e){
            System.out.println("Could not find text file");
            e.printStackTrace();
            return -1;
        }catch (Exception e) {
            e.printStackTrace();
            return -1;
        } 
        return numberOfLines;
    }
}
