package text;

import java.io.File;

public class FileMetaData {
    private final String FILEPATH = "./resources/Dracula.txt";
    private File file; 

    /**
     * Default constructor uses dracula.txt as file.
     */
    public FileMetaData(){
        file = new File(this.FILEPATH);
    }

    /**
     * @return the size of the file in bytes. Returns -1 if the file does not exists or an error occurs.
     */
    public long fileSize() {
        long lenght = -1;
        try {
            if(this.file.exists()){
                lenght = this.file.length();
            }
        }catch (SecurityException e) {
            e.printStackTrace();
        }
        return lenght;
    }

    /**
     * @return the name of the file. If file does not exist, or the program is not allowed to read it, an empty String is returned
     */
    public String getFilename(){
        String result = "";
        try {
            if(this.file.exists()) {
                result = this.file.getName();
            }
        }catch (SecurityException e) {
            e.printStackTrace();
        }
        return result;
    }
}
