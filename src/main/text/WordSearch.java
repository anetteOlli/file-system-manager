package text;

import java.util.Scanner;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
import java.util.List;
import java.util.regex.MatchResult;
import java.util.stream.Collectors;
import java.io.File;
import java.io.FileNotFoundException;



public class WordSearch {
    private final String FILEPATH = "./resources/Dracula.txt";

    /**
     * @param word string that will be searched for. Ignores lower/upper case.
     * @return number of times the word occured in the text.
     * @throws IllegalArgumentsException if the word argument makes the regular expression fail at compiling
     */
    public int wordCount(String word) throws IllegalArgumentException {
        int result = -1; 
       
        try (Scanner sc = new Scanner(new File(FILEPATH))) {
            Pattern pat =   Pattern.compile( "(?i)" + word);

            List<String> capWords = sc.findAll(pat).map(MatchResult::group).collect(Collectors.toList());
            result = capWords.size();
        }catch (FileNotFoundException e) {
            e.printStackTrace();
        }catch (PatternSyntaxException e) {
            throw new IllegalArgumentException("wordCount recieved illegal argument: word", e);
        }catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * @param word string that will be searched for. Ignores lower/upper case.
     * @return true if the word is found, false if the word is not found or if an error occured.
     * @throws IllegalArgumentsException if the word argument makes the regular expression fail at compiling
     */
    public boolean containWord(String word) throws IllegalArgumentException {
        boolean result = false;

        try (Scanner sc = new Scanner(new File(FILEPATH))) {
            Pattern pat = Pattern.compile("(?i)" + word);
            sc.useDelimiter(pat);
            if(sc.hasNext()) {
                sc.next();
            }
            if(sc.hasNext()) {
                result = true;
            }
        }catch(FileNotFoundException e) {
            e.printStackTrace();
        }catch(PatternSyntaxException e) {
            throw new IllegalArgumentException("wordCount recieved illegal argument: word", e);
        }catch (Exception e) {
            e.printStackTrace();
        }
        return result;  
    }
}